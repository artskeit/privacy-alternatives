# privacy-alternatives

A list of privacy friendly alternatives for useful services/tools/apps.

## Shortcuts

- [Gmail/Outlook](#gmail-outlook)
- [YouTube](#youtube)

## Gmail/Outlook

- [Tutanota](https://tutanota.com) - Own domain can be used when subscribed to a paid-plan. Applications for Desktop/iOS/Android are available.
- [ProtonMail](https://protonmail.com) - Own domain can be used when subscribed to a paid-plan. Applications for Desktop/iOS/Android are available.

## YouTube

A few different alternatives are available to be able to watch YouTube videos, most of them offer a different frontend.

### Platforms

- [PeerTube](https://peertube.tv) - ActivityPub-federated video streaming platform using P2P

### Applications

#### Desktop

- [FreeTube](https://freetubeapp.io) - A private YouTube client for Mac/Windows/Linux

#### iOS

There are (to my knowledge) no real YouTube clients available from the AppStore. There are some web-based instances which can be installed as a [PWA](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps).

See [Web](#web).

#### Android

- [NewPipe](https://newpipe.net) - A private YouTube client.

#### Web

- [Invidious](https://github.com/iv-org/invidious) - A web-based YouTube frontend. Self-hosting is possible.
- [Piped](https://github.com/TeamPiped/Piped) - A web-based YouTube frontend. Self-hosting is possible.
